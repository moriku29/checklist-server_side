package com.rikum.checklist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rikum.checklist.repositories.user.User;
import com.rikum.checklist.repositories.user.UserRepository;
import com.rikum.checklist.security.JwtTokenUtil;
import com.rikum.checklist.security.JwtUserDetailsService;
import com.rikum.checklist.wrappers.JwtRequest;
import com.rikum.checklist.wrappers.JwtResponse;
import com.rikum.checklist.wrappers.UserWrapper;

@Controller
@RequestMapping(path = "/api/user")
public class ChecklistController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	public ChecklistController() {
	}

	public ChecklistController(UserRepository userRepo) {
		this.userRepository = userRepo;
	}

	@PostMapping(path = "/add")
	public @ResponseBody ResponseEntity<?> createUser(@RequestBody UserWrapper user) throws Exception {
		if (user != null && user.getPassword() != null && user.getUsername() != null) {
			
			return ResponseEntity.ok(userDetailsService.save(user));
		}
		throw new Exception("Invalid Request");
	}
	
	@PostMapping(path="/login")
	public @ResponseBody ResponseEntity<?> login(@RequestBody JwtRequest authenticationRequest) throws Exception {
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}
	
	@PostMapping(path="/logout")
	public @ResponseBody ResponseEntity logout(@RequestBody User user) {
		return null;
	}
	
	@DeleteMapping(path="/delete")
	public @ResponseBody String deleteUser(@RequestBody User user) {
		return null;
	}
	
	@GetMapping(path="/allusers")
	public @ResponseBody Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}
	
	@GetMapping(path="/testCall")
	public @ResponseBody String testAuthentication() {
		return "Success";
	}
	
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
